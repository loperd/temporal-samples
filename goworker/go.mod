module money-transfer/app

go 1.14

require (
	github.com/google/uuid v1.3.0
	go.temporal.io/sdk v1.14.0
)
